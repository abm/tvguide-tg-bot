import requests
import base64, hashlib, hmac, time
from urllib import urlencode, quote_plus

GUIATIVI_API_KEY = "_TU_CLAVE_PUBLICA_"
GUIATIVI_SECRET_KEY = "_TU_CLAVE_SECRETA_"
url_base = "http://api.guiativi.es/"
version_api = "v2"

response = requests.get('https://httpbin.org/ip')


def guiativi(method, operation):
    # Sign the request
    token_hmac = hmac.new(
        key=GUIATIVI_SECRET_KEY,
        msg=method+operation,
        digestmod=hashlib.sha256).digest()

    # Base64 encode the signature
    token_hmac = base64.encodestring(token_hmac).strip()

    # Make the signature URL safe
    urlencoded_token_hmac = quote_plus(token_hmac)

    url_string = "%s%s/%s/%s/%s" % (url_base, version_api, urlencoded_token_hmac, GUIATIVI_API_KEY, operation)

    print(url_string)

    return url_string


if __name__ == "__main__":
    # execute only if run as a script
    main()